package net.coolskies.flagsoftheworld;
import com.google.android.gms.ads.AdRequest;  
import com.google.android.gms.ads.AdSize;  
import com.google.android.gms.ads.AdView;  
import com.google.android.gms.ads.AdListener;  
import android.os.Bundle;  
import android.view.View;  
import android.view.ViewGroup;  
import android.widget.Button;  
import android.widget.RelativeLayout;  
 
 public class AdMobQtActivity extends org.qtproject.qt5.android.bindings.QtActivity  
 {  
   private static ViewGroup viewGroup;  
   private AdView mAdView;  
   private boolean adAdded = false;  
   @Override  
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mAdView = new AdView(this);
        mAdView.setAdUnitId("ca-app-pub-8447325238832941/9705012916");
        mAdView.setAdSize(AdSize.SMART_BANNER);

        View view = getWindow().getDecorView().getRootView();

        if (view instanceof ViewGroup) {
            viewGroup = (ViewGroup) view;

            final RelativeLayout relativeLayout=new RelativeLayout(this);
            viewGroup.addView(relativeLayout);

            final RelativeLayout.LayoutParams adViewParams = new RelativeLayout.LayoutParams(
                    AdView.LayoutParams.WRAP_CONTENT,
                    150);
            //important
            adViewParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            adViewParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            relativeLayout.addView( mAdView, adViewParams);

            mAdView.setAdListener( new AdListener() {
                public void onAdLoaded(){
                    if( adAdded)
                        return;
                    adAdded = true;
                }
            });

            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd( adRequest);
        }
    }
	
   @Override  
   public void onPause() {  
     mAdView.pause();  
     super.onPause();  
   }  
   @Override  
   public void onResume() {  
     super.onResume();  
     mAdView.resume();  
   }  
   @Override  
   public void onDestroy() {  
     mAdView.destroy();  
     super.onDestroy();  
   }  
 }  