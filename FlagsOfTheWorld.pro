# allows to add DEPLOYMENTFOLDERS and links to the V-Play library and QtCreator auto-completion
TEMPLATE = app
CONFIG += v-play resources_big
QT += sql multimedia


qmlFolder.source = qml
#DEPLOYMENTFOLDERS += qmlFolder # comment for publishing

android {
    data.files = db/flags.sqlite
    data.path = /assets/db
    INSTALLS += data

    flags.files = assets/img/flags/*
    flags.path = /assets/img/flags
    INSTALLS += flags

    maps.files = assets/img/maps/*
    maps.path = /assets/img/maps
    INSTALLS += maps
}
else {
    dbFolder.source = db
    DEPLOYMENTFOLDERS += dbFolder

    flagsFolder.source = assets/img/flags
    flagsFolder.target = assets/img/
    DEPLOYMENTFOLDERS += flagsFolder

    mapsFolder.source = assets/img/maps
    mapsFolder.target = assets/img/
    DEPLOYMENTFOLDERS += mapsFolder
}

# Add more folders to ship with the application here

RESOURCES += resources.qrc

# NOTE: for PUBLISHING, perform the following steps:
# 1. comment the DEPLOYMENTFOLDERS += qmlFolder line above, to avoid shipping your qml files with the application (instead they get compiled to the app binary)
# 2. uncomment the resources.qrc file inclusion and add any qml subfolders to the .qrc file; this compiles your qml files and js files to the app binary and protects your source code
# 3. change the setMainQmlFile() call in main.cpp to the one starting with "qrc:/" - this loads the qml files from the resources
# for more details see the "Deployment Guides" in the V-Play Documentation

# during development, use the qmlFolder deployment because you then get shorter compilation times (the qml files do not need to be compiled to the binary but are just copied)
# also, for quickest deployment on Desktop disable the "Shadow Build" option in Projects/Builds - you can then select "Run Without Deployment" from the Build menu in Qt Creator if you only changed QML files; this speeds up application start, because your app is not copied & re-compiled but just re-interpreted


# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    flagsdatabasecontroller.cpp \
    flag.cpp \
    settings.cpp

android {
    DEFINES += ANDROID
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
    OTHER_FILES += android/AndroidManifest.xml
    OTHER_FILES += android/net/coolskies/flagsoftheworld/AdMobQtActivity.java \
}

ios {
    QMAKE_INFO_PLIST = ios/Project-Info.plist
    OTHER_FILES += $$QMAKE_INFO_PLIST
}

# set application icons for win and macx
win32 {
    RC_FILE += win/app_icon.rc
}
macx {
    ICON = macx/app_icon.icns
}

HEADERS += \
    flagsdatabasecontroller.h \
    flag.h \
    globalpaths.h \
    settings.h
