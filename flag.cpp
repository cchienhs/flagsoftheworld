#include "flag.h"

Flag::Flag( QObject *parent )
    : QObject( parent )
{

}

Flag::Flag( const Flag& rhs )
    : QObject()
{
    *this = rhs;
}

void Flag::setImageUrl( const QString& imageUrl )
{
    mImageUrl = imageUrl;
}

QString Flag::imageUrl() const
{
    return mImageUrl;
}

void Flag::setCountry( const QString& country )
{
    mCountry = country;
}

QString Flag::country() const
{
    return mCountry;
}

void Flag::setMapUrl( const QString& mapUrl )
{
    mMapUrl = mapUrl;
}

QString Flag::mapUrl() const
{
    return mMapUrl;
}

void Flag::setGeography( const QString& geography )
{
    mGeography = geography;
}

QString Flag::geography() const
{
    return mGeography;
}


void Flag::setChoices( const QStringList& choices )
{
    mChoices = choices;
}

QStringList Flag::choices() const
{
    return mChoices;
}

Flag& Flag::operator=(const Flag& c) {
    this->mImageUrl = c.mImageUrl;
    this->mCountry = c.mCountry;
    return *this;
}
