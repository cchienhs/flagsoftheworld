import QtQuick 2.0
import QtMultimedia 5.5

Rectangle {
    id: button
    // this will be the default size, it is same size as the contained text + some padding
    width: 200//buttonText.width+ paddingHorizontal*2
    height: buttonText.height+ paddingVertical*2

    color: "#e9e9e9"
    // round edges
    radius: 10

    // the horizontal margin from the Text element to the Rectangle at both the left and the right side.
    property int paddingHorizontal: 10
    // the vertical margin from the Text element to the Rectangle at both the top and the bottom side.
    property int paddingVertical: 10

    // access the text of the Text component
    property alias text: buttonText.text
    property alias color: button.color
    property alias textColor: buttonText.color
    property alias textSize: buttonText.font.pixelSize

    // this handler is called when the button is clicked.
    signal clicked

    SoundEffect {
            id: playSound
            source: "qrc:/sounds/assets/sounds/click.wav"
        }

    Text {
        id: buttonText
        anchors.centerIn: parent
        wrapMode: Text.WordWrap
        font.pixelSize: 18
        color: "black"
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: button.clicked()
        onPressed: {
            button.opacity = 0.5;
            playSound.play();
        }
        onReleased: button.opacity = 1
    }
}

