import QtQuick 2.0
import QtMultimedia 5.5

Rectangle {
    id: sideButton
    anchors.horizontalCenter: parent.horizontalCenter
    property string text: 'Button'
    property RadioGroup radioGroup
    radius: 10

    color: "#333333"

    SoundEffect {
            id: playSound
            source: "qrc:/sounds/assets/sounds/wooden_click.wav"
        }

    MouseArea {
        id: sideButtonMouseArea
        anchors.fill: sideButton
        onClicked: {
            sideButton.radioGroup.selected = sideButton;
            playSound.play();
        }
    }

    Text {
        id: sideButtonLabel
        text: sideButton.text
        font.pixelSize: Math.round(sideButton.height / 3)
        font.family: 'Tahoma'
        anchors.centerIn: sideButton
        color: radioGroup.selected === sideButton ? '#E2EBFC' : '#858585'
    }
}
