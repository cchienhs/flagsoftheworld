import QtQuick 2.0

Rectangle{
    id: quitDialog
    width: parent.width
    height: parent.height
    anchors.horizontalCenter: parent.horizontalCenter
    y: -100
    radius: 10
    color: "#80000000"

    property alias message: dialogDesc.text
    property alias yesButtonText: button_yes_text.text
    //property alias noButtonText: button_no_text.text

    property alias showDialog: showDialog.running
    property alias hideDialog: hideDialog.running

    signal yes_clicked

    // Quit dialog text
    Text{
        id: dialogDesc
        anchors.centerIn: parent
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: quitDialog.height * 0.12
    }

    /* Yes button */
    Button{
        id:yes_button
        width: 1/2 * parent.width
        height: 80
        color: "teal"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: button_yes_text
            anchors.centerIn: parent
            font.pixelSize: 32
            font.bold: true
            color: "white"
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                quitDialog.yes_clicked();
            }
        }
    }


    // Animating the quit dialog
    ParallelAnimation {
        id: showDialog
        PropertyAnimation { target: quitDialog; property: "opacity"; to: 1; duration: 800; easing.type: Easing.InQuad   }
        PropertyAnimation { target: quitDialog; property: "y"; duration: 800;
            from: -100; to: { parent.height/2 - quitDialog.height/2 }
            easing.type: Easing.InOutBack }
    }

    SequentialAnimation {
        id: hideDialog;
        PropertyAnimation { target: quitDialog; property: "opacity"; to: 0; duration: 500; easing.type: Easing.OutQuad}
        PropertyAnimation { target: quitDialog; property: "y"; to: -100; duration: 1 }
    }
}
