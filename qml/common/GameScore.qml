import QtQuick 2.0

Rectangle {
    id: container
    property alias fontSize: scoreText.font.pixelSize

    QtObject {
        id: internal
        property int score: 0;        
    }

    Text {
        id: scoreText
        text: internal.score
        font.pixelSize: 48
        color: "#e9e9e9"
    }

    function increaseScore() {
        internal.score += 1;
    }

    function resetScore() {
        internal.score = 0;
    }

    function getScore() {
        return internal.score;
    }
}
