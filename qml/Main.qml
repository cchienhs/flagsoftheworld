import VPlay 2.0
import QtQuick 2.0
import "scenes"

GameWindow {
    id: window
    width: 640
    height: 960

    // You get free licenseKeys from http://v-play.net/licenseKey
    // With a licenseKey you can:
    //  * Publish your games & apps for the app stores
    //  * Remove the V-Play Splash Screen or set a custom one (available with the Pro Licenses)
    //  * Add plugins to monetize, analyze & improve your apps (available with the Pro Licenses)
    licenseKey: "1966A12B3B69923D6DCAE2A518E2C277577898FE475C335438545ADC4C4B76374053448C26DCE3854BF0EE1D30F7686B80B5EE4078EA578C3D554B8EBD8C493F1030C51A55599AC77EF22A7E2956C5EF1700FBBBB974E81DB4208E6ADD6BE857A04879A9F055499BC07969903AE2D4774EAC5A1F1B2D7173E018B7D3560354AB7237FFA973E0190C76F25D465F768D8D9C3EA50C04B96B2D4AC230CE326ED701297C04ABE317D19D3CF4B7EC63219CD90640474954B231C0551C2AE6673C5CA62F80F8AE90488694ABCE6A5B7CA3B11EBD6B3217A56AF14470EBB3CB0F997362740BF306E5ECDA1C44F8B8EA64535B4DC6248945EFB29DB0291AA4D39D85DBACF259B3A948850189FB3C3667E1637E024035A6642B2E0B7BA11339AB676FC7DB7C3A1780B212A06A0296AEE002630DBF"
//"283D5925FB5F43F9F0468E60A012E26E28DA3D1ED73205BAA947075504E0C35456E448F48B79391FA32CF29095D816BA5C7DA94CE20B9FA693332FC177A51D78BE51F05CD19BFD914012E4AF3F324B666AF00B325232FB6186CE385183DC95D26633293F56C09750846201169C8689B7D15C93BDF941FB8F4E90451BB3FBE7CA9AE526D3A22B2E3797EDDDB0D87005F703D63297A3E44C7B5A01D96629A2A3E3B0DBF051BC29110D71F57B8E15F964DE48E99B09A680DC05E973CCC21FD06CFE884B77B0557AE68A5A61F4AF12F4B4900AFCCEFBFE913C47D71812C64B1D26D2B6017302846B052D01A036B127C2476CCD4F0F787EFC2F307A6CA4A42E08B58F8C6E600CCF63C55A904038371E7F01308A98CAD81769B912DE25656D9D8C7D7133A500720DF67FBF8FDA13F41C5BC63C"//
    // create and remove entities at runtime
    EntityManager {
        id: entityManager
    }

    // menu scene
    MenuScene {
        id: menuScene
        // listen to the button signals of the scene and change the state according to it
        onSelectPlayPressed: window.state = "game"
        onLearnPressed: window.state = "learn"
        // the menu scene is our start scene, so if back is pressed there we ask the user if he wants to quit the application
        onBackButtonPressed: {
            nativeUtils.displayMessageBox(qsTr("Really quit the game?"), "", 2);
        }
        onSelectScorePressed: window.state = "score"
        // listen to the return value of the MessageBox
        Connections {
            target: nativeUtils
            onMessageBoxFinished: {
                // only quit, if the activeScene is menuScene - the messageBox might also get opened from other scenes in your code
                if(accepted && window.activeScene === menuScene)
                    Qt.quit()
            }
        }
    }

    // scene for selecting levels
    SelectLevelScene {
        id: selectLevelScene
        onLevelPressed: {
            // selectedLevel is the parameter of the levelPressed signal
            gameScene.setLevel(selectedLevel)
            window.state = "game"

        }
        onBackButtonPressed: window.state = "menu"
    }

    // credits scene
    LearnScene {
        id: learnScene
        onBackButtonPressed: window.state = "menu"
    }

    // game scene to play a level
    GameScene {
        id: gameScene
        width: parent.width
        height: parent.height
        onBackButtonPressed: window.state = "menu"
        onGameEnded: {
            scoreScene.reportScore( score );
            window.state = "menu";
        }
    }

    ScoreScene {
        id: scoreScene
        onBackButtonPressed: window.state = "menu"
    }

    // menuScene is our first scene, so set the state to menu initially
    state: "menu"
    activeScene: menuScene

    // state machine, takes care reversing the PropertyChanges when changing the state, like changing the opacity back to 0
    states: [
        State {
            name: "menu"
            PropertyChanges {target: menuScene; opacity: 1}
            PropertyChanges {target: window; activeScene: menuScene}
        },
        State {
            name: "game"
            PropertyChanges {target: gameScene; opacity: 1}
            PropertyChanges {target: window; activeScene: gameScene}
        },
        State {
            name: "learn"
            PropertyChanges {target: learnScene; opacity: 1}
            PropertyChanges {target: window; activeScene: learnScene}
        },
        State {
            name: "score"
            PropertyChanges {target: scoreScene; opacity: 1}
            PropertyChanges {target: window; activeScene: scoreScene}
        }
    ]
}

