import VPlay 2.0
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import VPlayApps 1.0
import "../common"

SceneBase {
    id:root
    property bool menuShown: false
    anchors.fill: parent.gameWindowAnchorItem
    onVisibleChanged: {
        if( root.visible ) {
            if( d.currentCountry == "" ) {
                countryListView.currentIndex = 0;
                d.currentCountry = countryListView.currentItem.text;
            }
        }
    }

    QtObject {
        id: d
        property int menuTranslation: 200
        property int menuTranslationDuration: 300
        property string currentCountry: ""
        property string mapUrl:""
        property string flagUrl: ""
        property string geography: ""
        onCurrentCountryChanged: {
                var flag = dbController.flag( currentCountry );
                mapUrl = globalPaths.mapsDir + flag.mapUrl;
                flagUrl = globalPaths.flagsDir + flag.imageUrl;
                geography = flag.geography
        }
    }

    //main background
    Rectangle {

        anchors.fill: parent.gameWindowAnchorItem
        color: "#000000"
        /* this is what moves the normal view aside */
        transform: Translate {
            id: gameTranslate
            x: 0
            Behavior on x { NumberAnimation { duration: d.menuTranslationDuration; easing.type: Easing.OutQuad } }
        }

        //MainBar
        Rectangle {
            id: mainBar
            color: "transparent"
            width: root.width
            height: banner.height

            TopBanner{
                id: banner
                anchors.top: parent.top
                font.pixelSize: 24
            }

            Image {
                id: menuButton
                source: "qrc:/assets/img/menu.png"
                fillMode: Image.Stretch
                anchors {left: parent.left; top: banner.top; margins:5 }
                transform: Rotation {
                    id: imageRotation
                    origin.x: menuButton.x + menuButton.width/2
                    origin.y: menuButton.y + menuButton.height/2
                    angle: 0
                    Behavior on angle { NumberAnimation{ duration:50; }}
                }
                width: 25
                height: 25
                smooth: true
                MouseArea {
                    id: menuButtonMouseArea ;
                    anchors.fill: parent;
                    onClicked: root.onMenu();

                }
            }
        }

        Rectangle {
            anchors.top: mainBar.bottom
            width: parent.width
            height: parent.height
            color: "#222222"
            Image {
                id: map
                source: d.mapUrl;
                anchors.margins: 20
                width: parent.width
                height: 4/5 * parent.width
                fillMode: Image.Stretch
            }
            Image {
                anchors {left: map.left}
                width: 1/7 * map.width
                height: 2/3 * width
                fillMode: Image.Stretch
                source: d.flagUrl
            }
            Flickable {
                id: flick
                width: parent.width
                height: parent.height - map.height - 150;
                contentHeight: text.height
                anchors {top: map.bottom; margins: 10}
                clip:true
                Text {
                    id: text
                    width: parent.width
                    text: d.geography
                    wrapMode: TextEdit.WordWrap
                    color: "white"

                }
            }
            SwipeArea {
                    id: mouse
                    anchors.fill: parent
                    onSwipe: {
                        var count = countryListView.count;
                        switch (direction) {
                            case "right":
                                if ( countryListView.currentIndex == 0 )
                                    countryListView.currentIndex = count-1;
                                else
                                    countryListView.currentIndex--;
                                break;
                            case "left":
                                if ( countryListView.currentIndex == (count-1) )
                                    countryListView.currentIndex = 0;
                                else
                                    countryListView.currentIndex++;
                                break;
                        }
                        d.currentCountry = countryListView.currentItem.text;
                    }
                }

        }

    }

    /* this rectangle contains the "menu" */
    Rectangle {
        id: menuView        
        width: d.menuTranslation
        height: parent.height
        color: "#dddddd"
        opacity: root.menuShown ? 1 : 0;
        enabled: root.menuShown;
        Behavior on opacity { NumberAnimation{ duration: d.menuTranslationDuration * 0.8 } }

        ListView {
            id: countryListView
            anchors.fill: parent
            highlightMoveVelocity: 1000
            model: dbController.allCountries()
            delegate:countryNameDelegate
            highlight: Rectangle { width: countryListView.width; color: "#777777"; opacity: 0.6}
        }
    }

    Component {
        id: countryNameDelegate

        Text {
            id: countryNameDelegate_text
            text: modelData;
            font.pixelSize: 18
            width: countryListView.width
            wrapMode: Text.Wrap
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    countryListView.currentIndex = index;
                    d.currentCountry = countryNameDelegate_text.text;
                    onMenu();
                }
            }
        }
    }


    function onMenu()
    {
        if( root.menuShown ) {
            gameTranslate.x = 0;
            imageRotation.angle = 0;
        }
        else {
            gameTranslate.x = d.menuTranslation;
            imageRotation.angle = 90;
        }
        root.menuShown = !root.menuShown;
    }


}

