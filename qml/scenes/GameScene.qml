import VPlay 2.0
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.1
import "../common"

SceneBase {
    id:gameScene
    signal gameEnded( int score )

    onVisibleChanged: {
        if(gameScene.visible) {
            retrieveNewFlag();
        }
    }

    //private data struct
    Item {
        id: flagContainer
        property var flag:  {}
    }

    // background
    Rectangle {
        anchors.fill: parent.gameWindowAnchorItem
        color: "#000000"
        width: gameScene.width
        height: gameScene.height

        TopBanner{
            id: topBanner
        }

        Image {
            id: imageHolder
            source: ""
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: topBanner.bottom
            anchors.topMargin: 40
            height: 1/3.8 * parent.height
            width: 3/2 * height
        }

        GameScore {
            id: gameScore
            anchors.right: parent.right
            anchors.rightMargin: 80
            anchors.topMargin: 40
            anchors.top: topBanner.bottom
            fontSize: 52
        }

        //Choices Radio Group
        RadioGroup {
            id: choiceSelectionGroup
        }

        Column {
            id: choicesColumn
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: imageHolder.bottom
            anchors.topMargin: 60
            width: 5/6 * imageHolder.width
            spacing: 3

            RadioButton {
                id: choice1
                text: ""
                radioGroup: choiceSelectionGroup
                width: parent.width
                height: 2/5 * 1/5 * gameScene.height
             }
             RadioButton {
                id: choice2
                text: ""
                radioGroup: choiceSelectionGroup
                width: parent.width
                height: 2/5 * 1/5 * gameScene.height
             }
             RadioButton {
                id: choice3
                text: ""
                radioGroup: choiceSelectionGroup
                height: 2/5 * 1/5 * gameScene.height
                width: parent.width
             }
             RadioButton {
                id: choice4
                text: ""
                radioGroup: choiceSelectionGroup
                width:parent.width
                height: 2/5 * 1/5 * gameScene.height
             }
        }

        //Next Button
        MenuButton {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 50
            anchors.top: choicesColumn.bottom
            height: 130
            width: imageHolder.width
            color: "#666666"
            textSize: 30
            textColor: "#ffffff"
            text: "Next"
            onClicked: onAnswered()
        }

        CustomMessageDialog{
            id: messageDialog
            width: 4/5 * parent.width
            height: 1/2 * width
            color: "#dddddd"
            message: ""
            y: -100
            yesButtonText: "OK"
            opacity: 0
            onYes_clicked: {
                hideDialog= true;
            }
        }

        CustomMessageDialog{
            id: gameEndedDialog
            width: 3/4 * parent.width
            height: 1/2 * width
            color: "#eeeeee"
            message: ""
            y: -100
            yesButtonText: "OK"
            opacity: 0
            onYes_clicked: {
                hideDialog= true;
                gameEnded( gameScore.getScore() );
                gameScore.resetScore();
            }
        }
    }

    function onAnswered() {
        if( choiceSelectionGroup.selected == null ) {
            showMessageDialog( "Pls select a choice" );
        }
        else {
            //check answer
            var selectedAnswer = choiceSelectionGroup.selected.text;
            var realAnswer = flagContainer.flag.country;

            if( selectedAnswer === realAnswer ) {
                gameScore.increaseScore();
                retrieveNewFlag();
            }
            else {
                showGameEndedDialog( "Wrong answer: " + flagContainer.flag.country + "(Correct)<p> Game Ended - Score: " + gameScore.getScore() + "</p>" );
            }
        }
    }

    function showMessageDialog( message ) {
        messageDialog.message = message;
        messageDialog.showDialog = true;
    }

    function showGameEndedDialog( message ) {
        gameEndedDialog.message = message;
        gameEndedDialog.showDialog = true;
    }


    function retrieveNewFlag() {
        flagContainer.flag = dbController.flag();
        imageHolder.source = globalPaths.flagsDir + flagContainer.flag.imageUrl;
        choiceSelectionGroup.selected = null;
        var choices = flagContainer.flag.choices;
        choice1.text = choices[0];
        choice2.text = choices[1];
        choice3.text = choices[2];
        choice4.text = choices[3];
    }

}

