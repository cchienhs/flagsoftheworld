import VPlay 2.0
import QtQuick 2.0
import QtMultimedia 5.5
import "../common"

SceneBase {
    id: menuScene

    signal selectPlayPressed
    signal learnPressed
    signal selectScorePressed

    onVisibleChanged: {
        if( menuScene.visible ) {
            if( globalSettings.musicEnabled)
                music.play();
        }
    }

    QtObject {
        id: internal
        property int ballStartY: logoText.y + logoText.height + 30
        property int ballStartX : menuScene.width
        property int ballEndX : -200
        property int ballMaxY: ballStartY + 20
        property int ballMinY: ballStartY - 20
        property int xAnimDuration: 10000
        property int yAnimDuration: 1000
    }

    Audio {
        id: music
        loops: Audio.Infinite
        source: "qrc:/sounds/assets/sounds/main.wav"
    }


    // background
    Rectangle {
        anchors.fill: parent.gameWindowAnchorItem
        color: "#000000"


        // the "logo"
        Text {
            id: logoText
            anchors.horizontalCenter: parent.horizontalCenter
            y: 30
            font.pixelSize: 30
            color: "#e9e9e9"
            text: "Flags Of The World"
        }

        Image {
            id: logoImage
            source: "qrc:/assets/img/flags_of_the_world.png"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: logoText.bottom
            anchors.topMargin: 20
            width: parent.width * 1/2
            height: parent.width * 1/2

        }

        Image {
           id: ball
           width: 96
           height: 24
           x: parent.width; y: internal.ballStartY
           source: "qrc:/assets/img/airplane_with_banner.png"

           ParallelAnimation {
               running: true

               SequentialAnimation {
                   loops: -1
                   NumberAnimation{
                       target: ball
                       property: "x"
                       from: internal.ballStartX
                       to: internal.ballEndX
                       duration: internal.xAnimDuration
                   }
                   PauseAnimation { duration: 300 }
               }

               SequentialAnimation {
                   loops: -1
                   NumberAnimation {
                       target: ball
                       property: "y"
                       duration: 0.5 * internal.yAnimDuration
                       from: internal.ballMaxY
                       to: internal.ballMinY
                   }
                   NumberAnimation {
                       target: ball
                       property: "y"
                       duration: 0.5 * internal.yAnimDuration
                       from: internal.ballMinY
                       to: internal.ballMaxY
                   }
               }
           }
        }

        // menu
        Column {
            id: menu
            anchors.top: logoImage.bottom
            anchors.topMargin: 20
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 10
            MenuButton {
                color: "#66C0CF"
                textColor: "#ffffff"
                text: "Play"
                onClicked: selectPlayPressed()
            }
            MenuButton {
                color: "#66C0CF"
                textColor: "#ffffff"
                text: "Learn"
                onClicked: learnPressed()
            }
            MenuButton {
                color: "#66C0CF"
                textColor: "#ffffff"
                text: "Leaders Board"
                onClicked: selectScorePressed()
            }
        }

        Image {
            id: soundSettings
            source: globalSettings.musicEnabled?"qrc:/assets/img/music_on.png":
                                                 "qrc:/assets/img/music_off.png"
            width: 25
            height: 25
            anchors.left: menu.right
            anchors.top: menu.bottom
            anchors.margins: 10

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    globalSettings.musicEnabled = !globalSettings.musicEnabled
                    if( globalSettings.musicEnabled ) {
                        soundSettings.source = "qrc:/assets/img/music_on.png";
                        music.play();
                    }
                    else {
                        soundSettings.source = "qrc:/assets/img/music_off.png";
                        music.stop();
                    }
                }
            }
        }
    }
}

