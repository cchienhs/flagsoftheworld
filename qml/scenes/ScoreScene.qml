import VPlay 2.0
import QtQuick 2.0
import QtQuick.Controls 1.2
//import VPlayPlugins.facebook 1.0
import VPlayApps 1.0

import "../common"

SceneBase {

    id: scene
    width: 320
    height: 480

    function reportScore( score ) {
        gameNetwork.reportScore( score );
    }


    VPlayGameNetworkView {
      id: gameNetworkView
      anchors.fill: scene.gameWindowAnchorItem

      // no achievements used yet, so do not show the achievements icon
      showAchievementsHeaderIcon: false

      onBackClicked: {
        scene.backButtonPressed()
      }
    }

    VPlayGameNetwork {
      id: gameNetwork
      // received from the GameNetwork dashboard at http://gamenetwork.v-play.net
      gameId: 217
      secret: "6e2a0e916b1262df42d04ec9c21177e1"
      gameNetworkView: gameNetworkView

      onNewHighscore: {
        if(!isUserNameSet(userName)) {
          nativeUtils.displayTextInput("Congratulations!", "You achieved a new highscore. What is your player name for comparing your juicy scores?", "")
        }
        else {
          nativeUtils.displayMessageBox("Congratulations!", "You achieved a new highscore of "+gameScene.score+" points.", 1)
        }
      }
    }

    Connections {
      target: nativeUtils
      onTextInputFinished: {
        if(accepted) {
          var validUserName = gameNetwork.updateUserName(enteredText)
        }
      }
    }



}
