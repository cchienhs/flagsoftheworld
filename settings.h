#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>

struct SettingsPrivate;
class Settings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool musicEnabled READ musicEnabled WRITE setMusicEnabled NOTIFY musicEnabledChanged )
    Q_PROPERTY(bool soundEffectsEnabled READ soundEffectsEnabled WRITE setSoundEffectsEnabled )

public:
    Settings( QObject *parent=0 );
    virtual ~Settings();

    bool musicEnabled() const;
    void setMusicEnabled( const bool value );
    bool soundEffectsEnabled() const;
    void setSoundEffectsEnabled( const bool value );

signals:
    void musicEnabledChanged();

private:
    SettingsPrivate *d;
};

#endif // SETTINGS_H
