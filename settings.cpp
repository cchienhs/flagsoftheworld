#include <QSettings>
#include "settings.h"

struct SettingsPrivate
{
    QSettings *settings;
};

Settings::Settings( QObject *parent )
    : QObject( parent ),
      d( new SettingsPrivate )
{
    d->settings = new QSettings( "net.coolskies", "flagsoftheworld" );
}

Settings::~Settings()
{
    delete d->settings;
    delete d;
}

bool Settings::musicEnabled() const
{
    if( !(d->settings->contains("musicEnabled")))
       d->settings->setValue( "musicEnabled",QVariant(true));
    return d->settings->value( "musicEnabled" ).toBool();
}

void Settings::setMusicEnabled( const bool value )
{
    d->settings->setValue( "musicEnabled", QVariant(value) );

}

bool Settings::soundEffectsEnabled() const
{
    if( !d->settings->contains("soundEffectsEnabled"))
        d->settings->setValue( "soundEffectsEnabled", QVariant(true) );
    return d->settings->value( "soundEffectsEnabled" ).toBool();
}

void Settings::setSoundEffectsEnabled( const bool value )
{
    d->settings->setValue( "soundEffectsEnabled", QVariant(value) );
}
