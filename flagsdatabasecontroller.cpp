#include <Qimage>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>
#include <QDir>
#include <QFile>
#include <QStandardPaths>

#include "flagsdatabasecontroller.h"

struct FlagsDatabaseControllerPrivate
{
    QSqlDatabase database;
    unsigned int dbSize = 0;
};

FlagsDatabaseController::FlagsDatabaseController( const FlagsDatabaseController& rhs )
    : QObject(),
      d( new FlagsDatabaseControllerPrivate )
{
    d->database = rhs.d->database;
    d->dbSize = rhs.d->dbSize;
}

FlagsDatabaseController::FlagsDatabaseController( QObject *parent )
    : QObject( parent ),
      d( new FlagsDatabaseControllerPrivate )
{

    d->database = QSqlDatabase::addDatabase( "QSQLITE" );
#ifdef ANDROID
    QFile dfile("assets:/db/flags.sqlite");
    QString filePath = QStandardPaths::writableLocation( QStandardPaths::StandardLocation::AppLocalDataLocation );
    filePath.append( "/flags.sqlite");
    if (dfile.exists()) {
        if( QFile::exists( filePath ) )
            QFile::remove( filePath );

        if( dfile.copy( filePath ) )
            QFile::setPermissions( filePath, QFile::WriteOwner | QFile::ReadOwner );

    }
    d->database.setDatabaseName( filePath );
#else
    d->database.setDatabaseName( "db/flags.sqlite" );
#endif

    if( d->database.open() ) {
        qDebug( "DB open successful ");

        QSqlQuery q(d->database);
        q.exec("SELECT * from flags");
        q.last();
        d->dbSize = q.at() + 1;
    }
    else {
        qDebug( "DB failed" );
    }
}

FlagsDatabaseController::~FlagsDatabaseController()
{
    delete d;
}

QStringList FlagsDatabaseController::allCountries() const
{
    QStringList list;
    QSqlQuery q( d->database );
    q.prepare( "SELECT * FROM flags ORDER BY countryName ASC" );
    q.exec();

    int countryField = q.record().indexOf("countryName");
    while( q.next() ) {
        QString country = q.value(countryField).toString();
        list.append( country );

    }

    return list;
}

Flag* FlagsDatabaseController::flag( const QString& country ) const
{
    QSqlQuery q( d->database );
    q.prepare( "SELECT * FROM flags where countryName = ? LIMIT 1");
    q.bindValue( 0, country );
    q.exec();

    Flag *flag = new Flag;
    int countryField = q.record().indexOf("countryName");
    int imageField = q.record().indexOf("image");
    int geographyField = q.record().indexOf("geography");
    int mapField = q.record().indexOf("map");

    if( q.next() ) {
        QString geography = q.value( geographyField ).toString();
        QString map = q.value( mapField ).toString();
        QString country = q.value( countryField ).toString();
        QString flagImage = q.value( imageField ).toString();

        flag->setCountry( country );
        flag->setImageUrl( flagImage );
        flag->setGeography( geography );
        flag->setMapUrl( map );
    }
    return flag;
}

Flag* FlagsDatabaseController::flag() const
{
    int index;
    QSqlQuery q( d->database );

    int imageField;
    int countryField;
    Flag *result = new Flag();
    QStringList choices;

    while(true){
        index = qrand() % (d->dbSize);
        q.prepare( "SELECT * FROM flags WHERE id = ?" );
        q.bindValue( 0, index );

        q.exec();
        if( q.next()){
            imageField = q.record().indexOf("image");
            countryField = q.record().indexOf("countryName");


            QString imageUrl = q.value(imageField).toString();
            QString country = q.value(countryField).toString();
            result->setCountry( country );
            result->setImageUrl( imageUrl );
            choices.append( country );
            break;
        }
        else {
            continue;
        }
    }

    while( choices.length() < 4 ) {
        index = qrand() % d->dbSize;
        q.prepare( "SELECT * FROM flags WHERE id = ?" );
        q.bindValue( 0, index );
        q.exec();

        if( q.next() ) {
            QString country = q.value(countryField).toString();
            bool replicate = false;
            for( int i=0; i<choices.length(); ++i ) {
                if( choices[i] == country )
                    replicate = true;
            }

            if( replicate )
                continue;
            else
                choices.append( country);
        }
    }

    //jumble up the choices
    if( choices.length() > 1 ) {
        index = (qrand() % 4) ;

        if( index != 0 ) {
            QString temp = choices[index];
            choices[index] = choices[0];
            choices[0] = temp;
        }
    }

    result->setChoices( choices );
    return result;

}

