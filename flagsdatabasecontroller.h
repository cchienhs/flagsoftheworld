#ifndef FLAGSDATABASECONTROLLER_H
#define FLAGSDATABASECONTROLLER_H

#include <QObject>
#include <QSqlDatabase>
#include <QImage>
#include <QList>
#include "flag.h"

struct FlagsDatabaseControllerPrivate;
class FlagsDatabaseController : public QObject
{
    Q_OBJECT

public:
    FlagsDatabaseController( QObject *parent=0 );
    FlagsDatabaseController( const FlagsDatabaseController& rhs );
    virtual ~FlagsDatabaseController();

    Q_INVOKABLE Flag* flag() const;
    Q_INVOKABLE Flag* flag( const QString& country ) const;
    Q_INVOKABLE QStringList allCountries() const;

private:
    FlagsDatabaseControllerPrivate *d;
};

#endif // FLAGSDATABASECONTROLLER_H
