#ifndef GLOBALPATHS_H
#define GLOBALPATHS_H

#include <QObject>
#include <QDir>

class GlobalPaths : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString mapsDir READ mapsDir)
    Q_PROPERTY(QString flagsDir READ flagsDir)

public:
    GlobalPaths( QObject* parent=0)
        :QObject( parent )
    {}

    QString mapsDir() const
    {
#ifdef ANDROID
        return "assets:/img/maps/";
#else
        return  "file:" + QDir::currentPath() + "/assets/img/maps/";
#endif
    }

    QString flagsDir() const
    {
#ifdef ANDROID
        return "assets:/img/flags/";
#else
        return "file:" + QDir::currentPath() + "/assets/img/flags/";
#endif
    }
};

#endif // GLOBALPATHS_H
