#ifndef FLAG_H
#define FLAG_H

#include <QObject>
#include <QStringList>
#include <QImage>

class Flag : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString imageUrl READ imageUrl )
    Q_PROPERTY(QString country READ country )
    Q_PROPERTY(QString mapUrl READ mapUrl )
    Q_PROPERTY(QString geography READ geography )
    Q_PROPERTY(QStringList choices READ choices )

public:
    Flag( QObject *parent=0 );
    Flag( const Flag& rhs );

    void setImageUrl( const QString& imageUrl );
    QString imageUrl() const;

    void setCountry( const QString& country );
    QString country() const;

    void setMapUrl( const QString& mapUrl );
    QString mapUrl() const;

    void setGeography( const QString& geography );
    QString geography() const;

    void setChoices( const QStringList& choices );
    QStringList choices() const;

    virtual Flag& operator=(const Flag&);

private:
    QString mImageUrl;
    QString mCountry;
    QString mMapUrl;
    QString mGeography;
    QStringList mChoices;

};

#endif // FLAG_H
